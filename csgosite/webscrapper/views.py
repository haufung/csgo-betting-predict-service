from bs4 import BeautifulSoup
from django.http import HttpResponse, JsonResponse
from selenium import webdriver

from .controller import scrapPlayerFromFacitController
from .models import PlayerUrlFromHltv
from .utils import webDriverUtil, seleniumUtil


def scrapPlayerName(request):
    PlayerUrlFromHltv.objects.all().delete()
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    driver.get('https://www.hltv.org/stats/players')
    seleniumUtil.checkLoading(driver)
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    listOfUserName = soup.select(
        "body > div.bgPadding > div > div.colCon > div.contentCol > div.stats-section > table > tbody > tr > td.playerCol > a")
    for soupName in listOfUserName:
        url = f'https://www.faceit.com/en/players/{soupName.text}/stats/csgo'
        playerUrlObj = PlayerUrlFromHltv(name=soupName.text, url=url)
        playerUrlObj.save()

    for playerUrl in PlayerUrlFromHltv.objects.all():
        driver.get(playerUrl.url)
        seleniumUtil.checkLoading(driver)
        if driver.current_url == 'https://www.faceit.com/en/notfound':
            playerUrl.not_found_in_faceit = True
        else:
            playerUrl.not_found_in_faceit = False
        playerUrl.save()
    driver.quit()
    return HttpResponse("Hello, world. Scrapped Player Name and Generated Faceit Url")


def manuallyPatchFaceitUrl(request):
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    for playerUrl in PlayerUrlFromHltv.objects.filter(not_found_in_faceit=True, manually_found_in_faceit=None):
        driver.get(f'https://www.faceit.com/en/search/overview/{playerUrl.name}')
        seleniumUtil.checkLoading(driver)
        foundOrNot = input(f"Found {playerUrl.name} or not? (y/n)")
        while foundOrNot != 'y' and foundOrNot != 'n':
            print(f"Invalid Input {foundOrNot}")
            foundOrNot = input(f"Found {playerUrl.name} or not? (y/n)")

        if foundOrNot == 'y':
            playerUrl.manually_found_in_faceit = True
            playerUrl.url = driver.current_url + "/stats/csgo"
        else:
            playerUrl.manually_found_in_faceit = False
            playerUrl.url = None
        playerUrl.save()
    driver.quit()
    return HttpResponse("Manually Patch Player Url Done")


def scrapPlayerFromFacit(request):
    try:
        numberOfNamesScraped = scrapPlayerFromFacitController.startScrap()
    except Exception as error:
        return HttpResponse(f"{error}", status_code=500)
    return HttpResponse(f"Success! No. of player scrapped: {numberOfNamesScraped}")
