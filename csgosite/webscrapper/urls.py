from django.urls import path

from . import views

urlpatterns = [
    path('scrapPlayerName/', views.scrapPlayerName, name='scrapPlayerName'),
    path('scrapPlayerFromFacit/', views.scrapPlayerFromFacit, name='scrapPlayerFromFacit'),
    path('manuallyPatchFaceitUrl/', views.manuallyPatchFaceitUrl, name='manuallyPatchFaceitUrl')
]