from django.db import models

class PlayerUrlFromHltv(models.Model):
    name = models.TextField(primary_key=True)
    url = models.TextField(blank=True, null=True)
    not_found_in_faceit = models.BooleanField(blank=True, null=True)
    manually_found_in_faceit = models.BooleanField(blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.name

class PlayerUrlFromFaceit(models.Model):
    facit_username = models.TextField(primary_key=True)
    real_username = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    profile_url = models.TextField(blank=True, null=True)
    player_rank = models.TextField(blank=True, null=True)
    objects = models.Manager()

    def __str__(self):
        return self.facit_username

