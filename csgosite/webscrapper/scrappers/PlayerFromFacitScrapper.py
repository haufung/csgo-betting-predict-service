import time
import environ
import psycopg2
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from ..models import PlayerUrlFromFaceit

env = environ.Env()


def loginAuto(driver):
    emailText = driver.find_element_by_xpath(
        "/html/body/div[1]/div/div/div/auth-modal-react/div/div[2]/div[1]/form/div[1]/input")
    emailText.send_keys(env("FACEIT_EMAIL"))
    passwordText = driver.find_element_by_xpath(
        "/html/body/div[1]/div/div/div/auth-modal-react/div/div[2]/div[1]/form/div[2]/div[2]/input")
    passwordText.send_keys(env("FACEIT_PASSWORD"))
    loginButton = driver.find_element_by_xpath(
        "/html/body/div[1]/div/div/div/auth-modal-react/div/div[2]/div[1]/form/div[5]/button")
    loginButton.click()
    time.sleep(3)
    webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
    time.sleep(3)
    loginButton.click()
    time.sleep(3)


def scrapPlayersProfileInfo(driver):
    driver.get(f'https://www.faceit.com/en/dashboard/rankings')
    # seems checkLoading is not working after redirecting to new page, use sleep to work around first
    # seleniumUtil.checkLoading(driver)
    time.sleep(10)
    changeDistrictToEU(driver)

    rankingText = driver.find_element_by_xpath(
        '// *[ @ id = "home-main-height-wrapper"] / div / section / div / div / div / div[2] / div / h3 / span[2]')
    rankingText.click()

    soup = scrollDownPlayerListToDesiredNo(driver)

    nameSpanElements = soup.select(
        '#home-main-height-wrapper > div > section > div > div > div > div.mt-lg > table > tbody > tr > '
        'td.text-left > strong > span')
    countrySpanElements = soup.select(
        '#home-main-height-wrapper > div > section > div > div > div > div.mt-lg > table > tbody > tr > td.text-left '
        '> strong > flag > span > svg')

    removeInvalidPlayers(countrySpanElements, nameSpanElements)
    removeInvalidCountries(countrySpanElements, nameSpanElements)

    profileUrlTemplate = "https://www.faceit.com/en/players/"
    PlayerUrlFromFaceitObjects = []
    if len(nameSpanElements) != len(countrySpanElements):
        raise Exception("number of names found is not equal to countries! names: " + str(
            len(nameSpanElements)) + ", countries: " + str(len(countrySpanElements)))
    for x in range(len(nameSpanElements)):
        PlayerUrlFromFaceitObject = PlayerUrlFromFaceit(facit_username=nameSpanElements[x].text,
                                                        country=countrySpanElements[x].text,
                                                        profile_url=profileUrlTemplate + nameSpanElements[
                                                            x].text)
        PlayerUrlFromFaceitObjects.append(PlayerUrlFromFaceitObject)
    return PlayerUrlFromFaceitObjects


def scrollDownPlayerListToDesiredNo(driver):
    errorChecker = 0
    prevNumberOfPlayersLoaded = 0
    retry = 0
    while True:
        webdriver.ActionChains(driver).send_keys(Keys.END).perform()
        curPlayerList = BeautifulSoup(driver.page_source, 'html.parser').select(
            '#home-main-height-wrapper > div > section > div > div > div > div.mt-lg > table > tbody > tr > '
            'td.text-left > strong > span')
        curPlayerLen = len(curPlayerList)
        if curPlayerLen <= prevNumberOfPlayersLoaded:
            print("Number of Players did not increase after scroll!")
            errorChecker += 1
        if errorChecker > 20:
            if retry >= 6:
                print("retried 5 time and still failed, skip scrapping!")
                return []
            print("Number of Players did not increase after 20 scroll! Auto refresh page and refresh!")
            errorChecker = 0
            prevNumberOfPlayersLoaded = 0
            retry += 1
            webdriver.ActionChains(driver).send_keys(Keys.F5).perform()
        if (curPlayerLen >=
                int(env('NumberOfPlayerToScrap'))):
            return BeautifulSoup(driver.page_source, 'html.parser')


def removeInvalidCountries(countrySpanElements, nameSpanElements):
    lengthOfCountrySpan = len(countrySpanElements)
    x = 0
    while x < lengthOfCountrySpan:
        if countrySpanElements[x].text == '':
            del nameSpanElements[x]
            del countrySpanElements[x]
            lengthOfCountrySpan -= 1
        else:
            x += 1


def removeInvalidPlayers(countrySpanElements, nameSpanElements):
    lengthOfNameSpan = len(nameSpanElements)
    x = 0
    while x < lengthOfNameSpan:
        if nameSpanElements[x].text == '':
            del nameSpanElements[x]
            del countrySpanElements[x]
            lengthOfNameSpan -= 1
        else:
            x += 1


def changeDistrictToEU(driver):
    button = driver.find_element_by_css_selector('#home-main-height-wrapper > div > section > div > div > div > '
                                                 'div.mt-lg > div > div:nth-child(3) > select > option:nth-child(1)')
    button.click()
    wait = WebDriverWait(driver, 10)
    wait.until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="home-main-height-wrapper"]/div/section/div/div/div/div[2]/table/tbody/tr[1]')))


def insertDataToDB(playerUrlFromFaceitObjects):
    try:
        connection = psycopg2.connect("dbname=postgres user=postgres password=magCube host=localhost port=5432")
        cursor = connection.cursor()
        count = 0
        for PlayerUrlFromFaceitObject in playerUrlFromFaceitObjects:
            if PlayerUrlFromFaceitObject.facit_username == '':
                continue
            PlayerUrlFromFaceitObject.save()
            count += 1

        connection.commit()
        print(count, "Record inserted successfully into mobile table")

    except (Exception, psycopg2.Error) as error:
        if (connection):
            print("Failed to insert record into mobile table", error)

    finally:
        # closing database connection.
        if connection:
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
