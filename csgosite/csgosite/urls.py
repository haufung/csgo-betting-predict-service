from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('webscrapper/', include('webscrapper.urls')),
    path('admin/', admin.site.urls),
]