from datetime import datetime

import environ
from selenium import webdriver

from ..scrappers.PlayerFromFacitScrapper import loginAuto, scrapPlayersProfileInfo, insertDataToDB
from ..utils import webDriverUtil, seleniumUtil

env = environ.Env()


def startScrap():
    startTime = datetime.now()
    options, chrome_driver_path = webDriverUtil.webDriverSetup()
    driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
    driver.get(f'https://www.faceit.com/en/dashboard/rankings')
    seleniumUtil.checkLoading(driver)

    loginAuto(driver)

    PlayerUrlFromFaceitObjects = scrapPlayersProfileInfo(driver)

    scrapFinishedTime = datetime.now()
    driver.quit()

    print(f"time used: {scrapFinishedTime - startTime}")
    insertDataToDB(PlayerUrlFromFaceitObjects)

    return len(PlayerUrlFromFaceitObjects)
