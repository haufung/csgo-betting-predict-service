from django.contrib import admin

from .models import PlayerUrlFromHltv, PlayerUrlFromFaceit

admin.site.register(PlayerUrlFromHltv)
admin.site.register(PlayerUrlFromFaceit)